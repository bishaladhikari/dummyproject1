package com.android.dummyproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.android.dummyproject.databinding.ActivityMainBinding;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding activityMainBinding;
    private FragmentStateAdapter fragmentStateAdapter;
    private String[] titles = new String[]{"New Claim", "Checked Claim"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        fragmentStateAdapter = new PainterClaimAdapter(this);
        activityMainBinding.viewPager.setAdapter(fragmentStateAdapter);
        activityMainBinding.mainActivityToolbar.setNavigationIcon(R.drawable.backarrow);

        new TabLayoutMediator(activityMainBinding.tabLayout, activityMainBinding.viewPager,
                new TabLayoutMediator.TabConfigurationStrategy() {
                    @Override
                    public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                        tab.setText(titles[position]);
                    }
                }
        ).attach();

//        activityMainBinding.mainActivityToolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
        //activityMainBinding.tabLayout.setupWithViewPager(activityMainBinding.viewPager);
//        activityMainBinding.tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#573f73"));
//        new TabLayoutMediator(activityMainBinding.tabLayout, activityMainBinding.viewPager,
//                new TabLayoutMediator.TabConfigurationStrategy() {
//                    @Override
//                    public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
//                        tab.setText(titles[position]);
//                    }
//                }).attach();
    }

    private class PainterClaimAdapter extends FragmentStateAdapter {

        public PainterClaimAdapter(FragmentActivity fa) {
            super(fa);
        }


        @Override
        public Fragment createFragment(int pos) {
            switch (pos) {
                case 0: {
                    activityMainBinding.tabLayout.getTabAt(0).select();
                    return PainterClaimFragment.newInstance("fragment 1");

                }
                case 1:
                    activityMainBinding.tabLayout.getTabAt(1).select();
                    return PainterCheckedClaimFragment.newInstance("fragment 2");
                default:
                    activityMainBinding.tabLayout.getTabAt(0).select();
                    return PainterClaimFragment.newInstance("fragment 1, Default");
            }
        }

        @Override
        public int getItemCount() {
            return 2;
        }
    }
}