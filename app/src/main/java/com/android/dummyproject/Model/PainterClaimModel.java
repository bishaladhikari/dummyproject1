package com.android.dummyproject.Model;

public class PainterClaimModel {
    private int id;
    private String name;
    private String productName;
    private String liter;
    private String dateTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getLiter() {
        return liter;
    }

    public void setLiter(String liter) {
        this.liter = liter;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public PainterClaimModel(int id, String name, String productName, String liter, String dateTime) {
        this.id = id;
        this.name = name;
        this.productName = productName;
        this.liter = liter;
        this.dateTime = dateTime;
    }
}
