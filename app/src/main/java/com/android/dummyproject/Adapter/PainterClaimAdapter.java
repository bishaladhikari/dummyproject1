package com.android.dummyproject.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.dummyproject.ConfirmPainterClaimActivity;
import com.android.dummyproject.Model.PainterClaimModel;
import com.android.dummyproject.R;
import com.android.dummyproject.databinding.PainterClaimItemBinding;

import java.util.ArrayList;

public class PainterClaimAdapter extends RecyclerView.Adapter<PainterClaimAdapter.MyViewHolder> {
    public PainterClaimAdapter(ArrayList<PainterClaimModel> painterClaimModels) {
        this.painterClaimModels = painterClaimModels;
    }

    private ArrayList<PainterClaimModel> painterClaimModels;
    private LayoutInflater layoutInflater;
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        PainterClaimItemBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.painter_claim_item, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        PainterClaimModel painterClaimModel=painterClaimModels.get(position);
        holder.name.setText(painterClaimModel.getName());
        holder.productName.setText(painterClaimModel.getProductName());
        holder.liter.setText(painterClaimModel.getLiter());
        holder.dateTime.setText(painterClaimModel.getDateTime());
        holder.confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(view.getContext(), ConfirmPainterClaimActivity.class);
                intent.putExtra("name",""+painterClaimModels.get(position).getName());
                intent.putExtra("productName",""+painterClaimModels.get(position).getProductName());
                intent.putExtra("liter",""+painterClaimModels.get(position).getLiter());
                intent.putExtra("dateTime",""+painterClaimModels.get(position).getDateTime());
                view.getContext().startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return painterClaimModels.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,productName,dateTime,liter;
        public Button confirmBtn;
        public MyViewHolder(@NonNull final PainterClaimItemBinding painterClaimItemBinding) {
            super(painterClaimItemBinding.getRoot());
            name=painterClaimItemBinding.nameText;
            productName=painterClaimItemBinding.productName;
            dateTime=painterClaimItemBinding.dateTimeText;
            liter=painterClaimItemBinding.literText;
            confirmBtn=painterClaimItemBinding.comfirnBtn;

        }
    }
}
