package com.android.dummyproject;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.dummyproject.databinding.PainterCheckClaimFragmentBinding;

public class PainterCheckedClaimFragment extends Fragment {
    PainterCheckClaimFragmentBinding painterCheckClaimFragmentBinding;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        painterCheckClaimFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.painter_check_claim_fragment, container, false);
        View view = painterCheckClaimFragmentBinding.getRoot();
        return view;
    }
    public static PainterCheckedClaimFragment newInstance(String text) {

        PainterCheckedClaimFragment f = new PainterCheckedClaimFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}
