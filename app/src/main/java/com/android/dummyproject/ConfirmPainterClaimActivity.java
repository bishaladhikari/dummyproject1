package com.android.dummyproject;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.android.dummyproject.databinding.ActivityConfirmPainterClaimBinding;

public class ConfirmPainterClaimActivity extends AppCompatActivity {
    ActivityConfirmPainterClaimBinding activityConfirmPainterClaimBinding;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityConfirmPainterClaimBinding = DataBindingUtil.setContentView(this, R.layout.activity_confirm_painter_claim);
        activityConfirmPainterClaimBinding.confirmPainterClaimToolbar.setNavigationIcon(R.drawable.backarrow);
        activityConfirmPainterClaimBinding.confirmPainterClaimToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        activityConfirmPainterClaimBinding.cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        activityConfirmPainterClaimBinding.nameText.setText(getIntent().getStringExtra("name"));
        activityConfirmPainterClaimBinding.productName.setText(getIntent().getStringExtra("productName"));
        activityConfirmPainterClaimBinding.literText.setText(getIntent().getStringExtra("liter"));
        activityConfirmPainterClaimBinding.dateTimeText.setText(getIntent().getStringExtra("dateTime"));
    }
}
