package com.android.dummyproject;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.dummyproject.Adapter.PainterClaimAdapter;
import com.android.dummyproject.Model.PainterClaimModel;
import com.android.dummyproject.databinding.PainterClaimFragmentBinding;

import java.util.ArrayList;

public class PainterClaimFragment extends Fragment {
    PainterClaimFragmentBinding painterClaimFragmentBinding;
    PainterClaimAdapter painterClaimAdapter;
    private ArrayList<PainterClaimModel> painterClaimModels;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        painterClaimFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.painter_claim_fragment, container, false);
        View view = painterClaimFragmentBinding.getRoot();
        painterClaimFragmentBinding.recycleView.setHasFixedSize(true);
        painterClaimFragmentBinding.recycleView.setItemAnimator(new DefaultItemAnimator());
        painterClaimModels=new ArrayList<>();
        painterClaimModels.add(new PainterClaimModel(1,"Santosh Gurung","Ultra Protect Emulsion","20 Ltr.","12:07 AM,July 2021"));
        painterClaimModels.add(new PainterClaimModel(2,"Bishal Adhikari","Emulsion","15 Ltr.","01:00 AM,July 2021"));
        painterClaimModels.add(new PainterClaimModel(3,"Dilip Paudel","Protect Emulsion","00 Ltr.","12:30 AM,July 2021"));
        painterClaimModels.add(new PainterClaimModel(4,"Aasutosh Rimal","Ultra Protect","5 Ltr.","02:00 AM,July 2021"));
        painterClaimAdapter=new PainterClaimAdapter(painterClaimModels);
        painterClaimFragmentBinding.recycleView.setLayoutManager(new LinearLayoutManager(getContext()));
        painterClaimFragmentBinding.recycleView.setAdapter(painterClaimAdapter);
        return view;
    }
    public static PainterClaimFragment newInstance(String text) {

        PainterClaimFragment f = new PainterClaimFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}
